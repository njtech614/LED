/**
 * @file led.h
 * @brief  led驱动程序（基于STM32的v3.5库）
 * @author 王晓荣
 * @version 
 * @date 2014-03-07
 */
 
#ifndef __LED_H
#define __LED_H

#include "stm32f10x.h"                  // Device header

void     led_config(void);
void     led_on(uint8_t num);
void     led_off(uint8_t num);
void     leds_switch(uint32_t val);
uint8_t  led_get_num(void);

#endif 
