/**
 * @file main.c
 * @brief LED闪烁（基于STM32的v3.5库）
 * @author 王晓荣
 * @version 
 * @date 2014-03-19
 */

#include "led.h" 
#include "delay.h" 


/**
 * @brief 主程序
 *
 * @return 
 */
int main(void)
{
    led_config();    	
    while(1)
    {
        leds_switch(5);	
        delay(5000000); 		
        leds_switch(0);	
        delay(5000000); 
    }
}


