/**
 * @file gpio.c
 * @brief  gpio驱动程序（基于STM32的v3.5库）
 * @author 王晓荣
 * @version 
 * @date 2014-03-19
 */

#include "gpio.h"                  // Device header

/**
 * @brief 打开某GPIO的端口时钟
 *
 * @param GPIOx 某GPIO的端口时钟
 */
void gpio_clock_enable(GPIO_TypeDef* GPIOx)
{       
    if (GPIOx == GPIOA)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    }
    else if (GPIOx == GPIOB)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    }
    else if (GPIOx == GPIOC)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    }
    if (GPIOx == GPIOD)    
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
    }    
    else if (GPIOx == GPIOE)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
    } 
    else if (GPIOx == GPIOF)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);
    }
    else if (GPIOx == GPIOG)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG, ENABLE);
    } 
}
